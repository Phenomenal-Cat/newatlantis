.. |NAL| image:: _images/Logos/NewAtlantisLabs_1w.png
  :height: 60
  :class: no-scaled-link

.. panels::
  :card: shadow
  :img-top-cls: p-0 
  :column: col-lg-12 p-0 border-0
  :body: bg-dark
  :header: pl-4 pr-4 bg-primary
  
  ---
  :img-top: _images/Photos/GlassBrain_Cropcopy.png
  
  |NAL| 
  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  .. image:: _images/Photos/NMT_DecimatedBlue.gif
    :height: 150
    :align: right
    :class: no-scaled-link

  **Open-Source Solutions for Modern Neuroscience**

  New Atlantis Laboratories develops open-source :link-badge:`NA_SoftwareDropdown, software, ref, badge-success` and :link-badge:`NA_HardwareDropdown, hardware, ref, badge-info` designs for the neuroscience research community with the goal of accelerating scientific discovery. Conducting novel scientific research often requires the development of custom tools to address specific needs for which there are no commercially available solutions. Accessible, well documented, open-source solutions that permit sharing and modification provide researchers with more developed starting points, and are therefore critical to improving research efficiency. New Atlantis strives to assist the research community through the free and open distribution of knowledge and resources.


  .. image:: _images/Icons/osi_button.png
    :height: 30
    :target: https://opensource.org/

  .. image:: _images/Icons/oshw_button.png
    :height: 30
    :target: https://www.oshwa.org/

  .. image:: _images/Icons/CC_logo.png
    :height: 30
    :target: https://creativecommons.org/

  .. image:: _images/Icons/License_GPLv3.png
    :height: 30
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html

  .. image:: _images/Icons/Gitlab_button.png
    :height: 30
    :target: https://gitlab.com/Phenomenal-Cat

  .. image:: _images/Icons/GitHub_Icon.png
    :height: 30
    :target: https://github.com/Phenomenal-Cat

  .. image:: _images/Logos/readthedocs_Icon.png
    :height: 30 
    :target: https://readthedocs.org/projects/newatlantis/

  .. image:: _images/Icons/Thingiverse.png
    :height: 30
    :target: https://www.thingiverse.com/phenomenalcat/designs

  .. image:: _images/Icons/Vimeo.png
    :height: 30
    :class: no-scaled-link


.. |OS| image:: _images/Icons/osi_button.png
  :height: 30
  :target: https://opensource.org/

.. |OH| image:: _images/Icons/oshw_button.png
  :height: 30
  :target: https://www.oshwa.org/

.. |QM| image:: _images/Icons/Question-mark.png
  :height: 30
  :class: no-scaled-link

.. _NA_HardwareDropdown:

.. dropdown:: |OH| Open Hardware Projects
  :animate: fade-in
  :container: + shadow
  :open:
  :title: bg-warning text-dark text-left font-weight-bold p-2
  :body: bg-dark text-left

  New Atlantis Laboratories' :link-badge:`NA_Hardware, open hardware, ref, cls=badge-info text-white` projects are hosted on `Thingiverse <https://www.thingiverse.com/phenomenalcat/designs>`_ and licensed under the permissive `CERN open-hardware 2.0 <CERN-OHL-P>`_ license, while the accompanying documentation is hosted here on `ReadTheDocs <https://newatlantis.rtfd.io>`_ and licensed under `Creative Commons CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`_.

  .. panels::
    :container: container-lg pb-3
    :column: col-lg-3 col-md-4 col-sm-6 col-xs-12 p-2
    :img-top-cls: p-0 bg-dark
    :header: bg-white text-dark text-bold
    :body: bg-dark
    :footer: bg-dark


    ---
    :img-top: _images/Designs/PrimaThrone/NIF_MonkeyBox_front.png

    **PrimaThrone (MRI)**

    ^^^^^^^^^^^^

    Behavioral testing and functional imaging chairs for non-human primates.

    +++++++
    .. image:: _images/Logos/readthedocs_Icon.png
      :height: 30
    .. image:: _images/Icons/Thingiverse.png
      :height: 30
      :target: https://www.thingiverse.com/thing:2968729/files
    .. image:: _images/Logos/FreeCAD-logo.svg
      :height: 35
      :target: https://www.freecad.org

    ---
    :img-top: _images/Designs/PrimaThrone/PrimaThrone_V1_crop.jpeg

    **PrimaThrone (E.phys)**

    ^^^^^^^^^^^^
    Behavioral testing and electrophysiology chairs for non-human primates.

    .. link-button:: NA_EphysChair
      :text: 
      :type: ref
      :classes: stretched-link text-white

    +++++++
    .. image:: _images/Logos/readthedocs_Icon.png
      :height: 30
    .. image:: _images/Icons/Thingiverse.png
      :height: 30
      :target: https://www.thingiverse.com/thing:2968729/files
    .. image:: _images/Logos/FreeCAD-logo.svg
      :height: 35
      :target: https://www.freecad.org

    ---
    :img-top: _images/Designs/METHVEX/METHVEx_30deg.png

    **METHVEx**

    ^^^^^^^^^^^^
    Macaque Eye-Tracking for Head-free Visual Experiments.

    .. link-button:: NA_METHVEX
      :text: 
      :type: ref
      :classes: stretched-link text-white

    +++++++
    .. image:: _images/Logos/readthedocs_Icon.png
      :height: 30
    .. image:: _images/Icons/Thingiverse.png
      :height: 30
      :target: https://www.thingiverse.com/thing:2968729/files
    .. image:: _images/Logos/FreeCAD-logo.svg
      :height: 35
      :target: https://www.freecad.org
    .. image:: _images/Logos/PupilLabs_Icon.png
      :height: 30
      :target: https://pupil-labs.com/products/core/


    ---
    :img-top: _images/Designs/RestEasy/RestEasy_headerim.png

    .. image:: _images/Logos/RestEasy_b.svg
      :height: 30
      :class: no-scaled-link


    ^^^^^^^^^^^^
    A chin-rest design for head stabilization in human psychophysics research.

    .. link-button:: NA_RestEasy
      :text: 
      :type: ref
      :classes: stretched-link text-white
    
    +++++++
    .. image:: _images/Logos/readthedocs_Icon.png
      :height: 30
    .. image:: _images/Icons/Thingiverse.png
      :height: 30
      :target: https://www.thingiverse.com/thing:2968729/files
    .. image:: _images/Logos/FreeCAD-logo.svg
      :height: 35
      :target: https://www.freecad.org



.. _NA_SoftwareDropdown:
  
.. dropdown:: |OS| Open Software Projects
  :animate: fade-in
  :container: + shadow
  :open:
  :title: bg-warning text-dark text-left font-weight-bold p-2 font-size-h1
  :body: bg-dark text-left

  New Atlantis Laboratories' :link-badge:`NA_Software, open software, ref, cls=badge-success text-white` projects are hosted in publicly accessible online repositories on `GitLab <https://gitlab.com/Phenomenal-Cat>`_ and `GitHub <https://github.com/Phenomenal-Cat>`_, and licensed under the copyleft `GNU Public License v3.0 (GPL3) <https://www.gnu.org/licenses/gpl-3.0.en.html>`_.

  .. image:: _images/Icons/License_GPLv3.png
    :height: 30
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html

  
  .. panels::
    :container: container-lg pb-3
    :column: col-lg-3 col-md-4 col-sm-6 col-xs-12 p-2
    :img-top-cls: p-0 bg-dark
    :header: bg-white text-dark text-bold p-0 pl-3 pt-2
    :body: bg-dark
    :footer: bg-dark

    ---
    :img-top: _images/Photos/MF3D_CooCall.png

    .. image:: _images/Logos/MF3D_Logo.png
      :height: 60
      :target: https://mf3d.readthedocs.io/

    ^^^^^^^^^^^^
    Virtual macaque monkey avatar media and Blender software tools for studying social processing in non-human primates.

    .. link-button:: https://mf3d.readthedocs.io
      :text: 
      :classes: stretched-link text-white

    +++++++
    .. image:: _images/Logos/readthedocs_Icon.png
      :height: 30
      :target: https://mf3d.readthedocs.io/
    .. image:: _images/Icons/GitHub_Icon.png
      :height: 30
      :target: https://github.com/Phenomenal-Cat/MF3D-Tools
    .. image:: _images/Logos/Python_Icon.png
      :height: 30
      :target: https://www.python.org/
    .. image:: _images/Logos/Blender_Icon.png
      :height: 30
      :target: http://www.blender.org

    ---
    :img-top: _images/Photos/BrainSlice_bw.png

    .. image:: _images/Logos/Ignition.png
      :height: 60
      :target: https://gitlab.com/Phenomenal-Cat/IGNITION

    ^^^^^^^^^^^^
    Image-Guided Neurosurgical Implantation Tools for Invasive Open Neuroscience. A Slicer module.

    +++++++
    .. image:: _images/Logos/readthedocs_Icon.png
      :height: 30
      :target: https://mf3d.readthedocs.io/
    .. image:: _images/Icons/Gitlab_button.png
      :height: 30
      :target: https://gitlab.com/Phenomenal-Cat/IGNITION
    .. image:: _images/Logos/Python_Icon.png
      :height: 30
      :target: https://www.python.org/
    .. image:: _images/Logos/Slicer_Icon.png
      :height: 30
      :target: http://www.slicer.org

    ---
    :img-top: _images/Photos/NTB_GUImontage.png

    .. image:: _images/Logos/NTB_Logo_b.png
      :height: 60

    ^^^^^^^^^^^^
    Experimental control system for behavioral neuroscience

    .. link-button:: https://ntb.rtfd.io
      :text: 
      :classes: stretched-link text-white

    +++++++
    .. image:: _images/Logos/readthedocs_Icon.png
      :height: 30
      :target: https://ntb.rtfd.io
    .. image:: _images/Icons/GitHub_Icon.png
      :height: 30
      :target: https://github.com/Phenomenal-Cat/NIF-Toolbar
    .. image:: _images/Logos/Octave_Icon.png
      :height: 30
      :target: https://www.gnu.org/software/octave/index
    .. image:: _images/Logos/PTB_Icon.png
      :height: 30
      :target: http://psychtoolbox.org/

    ---
    :img-top: _images/Designs/MultidriveDesigner/Murphy_microdrive1.png

    .. image:: _images/Logos/MIDAS_placeholder.png
      :height: 60

    ^^^^^^^^^^^^
    Multidrive Implant Designer for Anatomical Specificity. A FreeCAD workbench for designing custom cranial implant hardware.

    .. link-button:: NA_MIDAS
      :type: ref
      :text: 
      :classes: stretched-link text-white

    +++++++
    .. image:: _images/Logos/readthedocs_Icon.png
      :height: 30
      :target: https://mf3d.readthedocs.io/
    .. image:: _images/Icons/GitHub_Icon.png
      :height: 30
      :target: https://github.com/Phenomenal-Cat/NIF-Toolbar
    .. image:: _images/Logos/Python_Icon.png
      :height: 30
      :target: https://www.python.org/
    .. image:: _images/Logos/FreeCAD-logo.svg
      :height: 35
      :target: https://www.freecad.org


.. dropdown:: |QM| About
  :animate: fade-in
  :container: + shadow
  :open:
  :title: bg-warning text-dark text-left font-weight-bold p-2
  :body: bg-dark text-left

  .. image:: _images/Photos/SirFrancisBacon.png
    :width: 20%
    :align: right
    :class: no-scaled-link

  .. image:: _images/Logos/OSHWA_LicenseInfo.svg
    :width: 20%
    :align: right

  **New Atlantis Laboratories** takes its name from the `1626 utopian novel <https://en.wikipedia.org/wiki/New_Atlantis>`_ by Sir Francis Bacon, in which he described a fictitious island where people held attitudes towards the free and open dissemination of knowledge that we refer to today as `'open science' <https://en.wikipedia.org/wiki/Open_science>`_. The goal of **New Atlantis Laboratories** is to make a range of niche digital tools developed for specific applications within behavioural neuroscience research publicly available to the research community. In addition to software tools this includes hardware designs released under permissive open-source licenses that allow commercial enterprises to acquire, develop, fabricate and retail these designs.


  .. image:: _images/Icons/osi_button.png
    :height: 30
    :target: https://opensource.org/

  .. image:: _images/Icons/oshw_button.png
    :height: 30
    :target: https://www.oshwa.org/

  .. image:: _images/Icons/CC_logo.png
    :height: 30
    :target: https://creativecommons.org/

  .. image:: _images/Icons/License_GPLv3.png
    :height: 30
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html

  .. image:: _images/Logos/CC.png
    :height: 30
    :target: https://creativecommons.org/licenses/by-nc-sa/4.0/


.. toctree::
   :hidden:
   :maxdepth: 3

   NA Hardware <NA_Hardware>
   NA Software <NA_Software>
   
