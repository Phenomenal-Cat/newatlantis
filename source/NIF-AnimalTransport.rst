.. _NIF_AnimalTransport:

===================================
NIF Animal Transport Hardware
===================================

Safely transporting animals from the Central Animal Facility to the NIF requires specialized equipment. The NIF has developed various hardware solutions to the specific challenges of awake NHP fMRI, which can be provided to NIF users as needed. 

.. contents:: :local:


CAF Transport Carts for Anesthetized Scans
=============================================

.. image:: ../_images/Photos/Hardware/CAF_AnimalTransportCart.jpg
  :width: 30%
  :align: right

Transportation of anesthetized animals should be performed using one of the CAF's dedicated transport tub carts, which are stored in the corridor outside the CAF surgical suite on the B1 level. The carts are manufactured by `Prairie View Industries Food Service <https://www.pvifs.com/products/list-products/section/2/category/11/>`_ from 1.5" diameter welded aluminum frame and hold seamless polyethylene tubs. The casters have been replaced with MR-compatible 5" wheels, so that the cart itself is safe to be brought into the magnet room. 

.. warning:: Caution must be taken to ensure that the collars and collar screws of anesthetized animals are MR-compatible, otherwise they must be removed during scanning. Similarly, check the transport tub for metal objects (tools, pens, needles, etc.) before entering the magnet room.


MRI trolley for NIF vertical chairs
======================================

The trolleys for transporting the vertical NHP chairs between the CAF and the NIF's 4.7T Bruker scanner are constructed from commercially available aluminum profile, custom machined aluminum plates and non-magnetic hardware. These carts can therefore be wheeled into the 4.7T scanner room and locked in place next to the elevator in order to minimize the distance when transferring animals. 

The table below lists the components used for construction of these trolleys. However, before ordering it should be noted that assembly involves the following additional steps:

- Ideally the parts should be welded together rather than held only with fasteners. This is because with repeated use (including both transport and the heating and cooling effect of cage washing) the fasteners will inevitably loosen and need to be tightened. 
- The plate aluminum needs to be drilled with four through-holes to accommodate 1/4"-20 bolts. This is easiest to achieve using a drill press.


.. csv-table::
  :file: ../_static/CSVs/NIF_Hardware_VerticalTrolley.csv
  :align: right
  :header-rows: 1
  :widths: auto




MRI trolley for NIF horizontal chairs
=======================================

.. image:: ../_images/Photos/Hardware/NIF_BevilTrolley_CAD.png
  :width: 30%
  :align: right

.. image:: ../_images/Photos/Hardware/NIF_BevilTrolley_photo.jpeg
  :width: 30%
  :align: right

The trolleys for transporting the horizontal NHP chairs between the CAF and the NIF's 3T Siemens scanner are constructed from commercially available aluminum profile and non-magnetic parts. These carts can therefore be wheeled into the 3T scanner room and locked in place next to the patient bed in order to minimize the distance when transferring animals. This trolley design positions the chair door at the correct height for transferring animals directly into the chair from the bottom row of `Allentown quad <https://www.allentowninc.com/large-animal-housing/primate/>`_ home cages.

The table below lists the components used for construction of these trolleys. However, before ordering it should be noted that assembly involves the following additional steps:

- All T-slot aluminium profile pieces need both end holes tapped with 1/4"-20 threads (except for the 2x1 profile). This optional service can be requested from vendors like `8020 <https://8020.net>`_ at an extra cost and slight additional delay to shipping. Alternatively, this can be done by hand using the appropriate `tap <https://www.mcmaster.com/2687A48/>`_, `wrench <https://www.mcmaster.com/2546A23/>`_, and `lubricant <https://www.mcmaster.com/1413K42/>`_.

- The four 22" long T-slot profile pieces need to have 1/4" through holes drilled through them as indicated by the red lines in the figure to the right. A T-slot drill jig can be purchased from `McMaster-Carr <https://www.mcmaster.com/47065T448/>`_ for use with a hand drill, but a drill press is recommended for this. A tip is to use the 2x2" plates to clamp pairs of 22" pieces together and drill together in order to ensure perfect alignment of through holes.

These customizations can alternatively be requested at the time of order from companies such such as `8020 <https://8020.net>`_ (although this will add to the `shipping time <https://8020.net/shipping-information>`_), or performed by the NIMH machine shop once the cut lengths are received.

.. csv-table::
  :file: ../_static/CSVs/NIF_Hardware_BevilTrolley.csv
  :align: right
  :header-rows: 1
  :widths: auto

Note that the most expensive components are the non-magnetic casters. An alternative option is to purchase an alternative caster (such as the regular version of the same MedCaster nylon 5" locking casters: `NG-05QDP125-TL-TP01 <https://www.zoro.com/medcaster-plate-caster-swivel-5-wheel-dia-ng05qdp125tltp01/i/G8094590/>`_), and transfer the NHP chair from the transport trolley to an MR-compatible trolley before entering the magnet room.


NIF horizontal training chairs
=======================================

.. image:: ../_images/Photos/Hardware/NIF_MonkeyBox_front.png
  :width: 30%
  :align: right

.. image:: ../_images/Photos/Hardware/NIF_MonkeyBox_rear.png
  :width: 30%
  :align: right

The NIF 'MonkeyBox' horizontal training chair was designed as a solution to the problem of  obtaining horizontal fMRI-compatible chairs quickly enough and in sufficient quantity to allow multiple research groups to train their animals for awake fMRI in the NIF. The outer dimensions and shape of the box are identical to those of the NIF's horizontal fMRI chairs, but the use of commercially available extruded aluminium profile frame and off-the-shelf hardware greatly simplifies assembly and speeds up construction. The panels are 3/8" polycarbonate, which can be custom cut to the required shapes (including bevel and mitre cuts) by vendors. The only custom machining required is the drilling of through-holes in the polycarbonate.

The chair design includes the following features:

- **Safety**

  - guillotine-style neck plate on the front panel
  - rear guillotine door with latches for locking in both open and shut positions
  - guillotine door will not fully close without user intervention (no trapped tails!)

- **Adjustability**

  - floor insert and drip tray, adjustable to multiple different heights
  - adjustable headpost crossbar with lever locks
  - optional response lever attachments to front panel

- **Ease of use**

  - handles on the top of the chair frame for ease of lifting
  - removable drip tray is easy to empty and can save the need for daily cage wash

The following bill of materials provides suggested vendors for purchasing the aluminium extrusion and hardware parts. Custom cut polycarbonate is available from various vendors suggested here:

- `eMachineSHop <https://www.emachineshop.com/quote/>`_
- `Big Blue Saw <https://www.bigbluesaw.com/>`_
- `FedTech <https://www.fedtech.com/request-for-quote.html>`_


.. csv-table::
  :file: ../_static/CSVs/NIF_Hardware_HorizontalTrainingChair.csv
  :align: right
  :header-rows: 1
  :widths: auto


In order to make this design MRI-compatible, it is recommended to use `fiberglass structural profile <https://mgs4u.com/product/1-on-side-square-tube/?v=7516fd43adaa>`_ in the place of the aluminium. Since fiberglass profile is not readily available with T-slots, all screw hole swill need to be drilled and tapped. However, this is a feasible approach to constructing a MRI-compatible chair with greater strength and rigidity than chairs where the polycarbonate panels are directly bolted or glued together.


Rogue Research chair modifications
=======================================

.. image:: ../_images/Photos/Hardware/NIF_RogueTrolleyFront.jpg
  :width: 30%
  :align: right

The NIF owns several `Rogue Research fMRI chairs <https://www.rogue-research.com/veterinary/mri-equipment/>`_ designed for awake macaque imaging experiments in the Siemens Prisma scanner. However, their default configuration is poorly suited to the specific constraints of the facility. We perform the following modifications to these chairs in order to make them safer and easier for NIF users:

- Top panel-mounted headpost holder
- Collar-less 'guillotine' style neck plate
- Custom trolley for horizontal position
- Secure attachment to Allentown quad cage doors
- Rear guillotine door latches for added safety

.. csv-table::
  :file: ../_static/CSVs/NIF_Hardware_RogueChairParts.csv
  :align: right
  :header-rows: 1
  :widths: auto