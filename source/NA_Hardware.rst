.. _NA_Hardware

.. |HW| image:: _images/Icons/oshw_button.png
  :height: 30
  :target: https://www.oshwa.org/

.. |SW| image:: _images/Icons/osi_button.png
  :height: 30
  :target: https://opensource.org/

====================
|HW| Open Hardware
====================

New Atlantis Laboratories' :link-badge:`NA_Hardware, open hardware, ref, cls=badge-info text-white` projects are hosted on `Thingiverse <https://www.thingiverse.com/phenomenalcat/designs>`_ and licensed under the permissive `CERN open-hardware 2.0 <CERN-OHL-P>`_ license, while the accompanying documentation is hosted here on `ReadTheDocs <https://newatlantis.rtfd.io>`_ and licensed under `Creative Commons CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`_.

Open Hardware Projects
----------------------------


.. panels::
  :column: col-lg-12 p-2 border-0
  :header: bg-info text-dark text-bold
  :body: bg-dark
   

  ---
  .. image:: _images/Logos/RestEasy_w.svg
    :align: left
    :height: 40

  .. image:: _images/Icons/Thingiverse.png
    :height: 30
    :align: right
    :target: https://www.thingiverse.com/thing:2968729/files


  **An open-source chin rest for human psychophysics research.**

  ^^^^^^^^^^^^

  Some stuff...

  ---

  **Transportation of laboratory animals.**
 
  ^^^^^^^^^^^^

  More stuff...



.. toctree::
   :maxdepth: 2
   :hidden:

   RestEasy - human chin rest <NA_RestEasy>
   PrimaThrone - NHP chair <NA_EphysChair>
   METHVEX - NHP head-free eye tracking <NA_METHVEX.rst>