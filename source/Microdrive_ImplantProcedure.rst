.. _McMahonImplant_

=====================================================================
Ide-McMahon Surgical Implantation Procedure
=====================================================================



Surgical Implantation Procedure
=====================================

.. panels:: 
  :column: col-lg-12 p-0
  :body: bg-dark text-justify text-light

    .. tab:: 1. Mark locations

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide1.png
        :align: right
        :width: 100%

      - Clean the skull surface
      - Position chamber using stereotax
      - Check clearance from headpost (with attachment)
      - Mark chamber location
      - Mark screw locations

    .. tab:: 2. Drill skull

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide2.png
        :align: right
        :width: 100%

      - Drill and tap screw holes
      - Insert ceramic screws
      - Mark craniotomy location by lowering guide tube and stylet in stereotax
      - Drill craniotomy

    .. tab:: 3. Insert guide tube

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide4.png
        :align: right
        :width: 100%

      - Lower guide tube and stylet on stereotaxic arm
      - Stop at appropriate target depth
      - Fill in the craniotomy around the guide tube with bone wax
      - Coat the bone surface with Copalite varnish to seal it

    .. tab:: 4. Place chamber

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide5.png
        :align: right
        :width: 100%

      - Slide the chamber down over the guide tube
      - Position the chamber base against the skull surface
      - Apply a thin layer of dental acrylic between the skull and the chamber

    .. tab:: 5. Build acrylic cap

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide6.png
        :align: right
        :width: 100%

      - Build up the dental acrylic around the chamber to cover the screws
      - Ensure that acrylic does not impede attachment of the cap
      - Make the contour of the acrylic as smooth as possible

    .. tab:: 6. Remove the stylet

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide7.png
        :align: right
        :width: 100%

      - Fix the guide tube in place with a small drop of glue
      - Once glue is dry, slowly remove the stylet
      - Cut the top of the guide tube diagonally, just below the top of the chamber

    .. tab:: 7. Insert the electrode

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide8.png
        :align: right
        :width: 100%

      - Mount the electrode (loaded into the microdive) onto the stereotaxic arm
      - Lower the electrode until the tip approaches the guide tube
      - Tie a loose loop of vicryl suture around the tip of the brush, to reduce the splay

    .. tab:: 8. Lower the drive

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide9.png
        :align: right
        :width: 100% 

      - Carefully lower the brush tip just below the top of the guide tube
      - Move the electrode in the M-L or A-P direction to get the wires into the diagonal cut
      - Lower until the microdrive is seated on the chamber

    .. tab:: 9. Secure microdrive

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide10.png
        :align: right
        :width: 100% 

      - Insert nylon screws to fix microdrive firmly to the chamber
      - Fill in around the guide tube-electrode interface with Kwik-Cast silicone

    .. tab:: 10. Advance microdrive

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide11.png
        :align: right
        :width: 100%

      - Turn the drive screw to lower the electrode
      - Lower until the electrode tip is <1mm inside the guide tube
      - Further electrode advancement should be done during neural recording

    .. tab:: 11. Secure cap

      .. image:: ../_images/Guides/IdeMcMahon_ImplantProcedure/Slide12.png
        :align: right
        :width: 100%

      - Lower cap over microdrive 
      - Run electrode wire under cap through wire channel in chamber
      - Secure the cap with set screws (careful not to press on wire)
      - Attach electrode connectors in dental acrylic




